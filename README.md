# Power BI dataset refresher

- **autor:** Tomas Votava (tomas.votava@bizztreat.com)
- **created:** 2020
- **modified:** 08-04-2024
- **changelog:**:
    - 08-04-2024: Added support for service principal authentization. Backwards compatibility ensured by defaulting to `user_account` if `auth_method` key is not found. (Michal Kral: michal.kral@bizztreat.com)

## Description

This script invokes Power BI datasets' refresh and waits until it sucessfully finishes.
This script will fail upon any error encountered.

## Setup
1. Create an app using https://app.powerbi.com/embedsetup
2. The app needs `Dataset.ReadWrite.All` and `Workspace.Read.All` permissions
3. Save `client_id` and `client_secret` that are generated for you upon creation.
4. After creating the app, the API rights usually need to be granted by your Azure Admin. App registrations -> <\your app> -> API permissions -> Grant

### What it is used for

Invoke Power BI dataset refresh via REST API

### What the code does

The program does:

1. Obtain OAuth2 access token
2. Start dataset refresh
3. Wait for it to end or fail

## Requirements

See _requirements.txt_

- python >= 3.5
- requests

## Usage

```console
python3 main.py [--debug] [--config <path>]
```

## Configuration

Program relies on config.json. Schema depends on the auth method you use. Samples can be found below.



| param                         | description                                                                                                                        |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| auth_method                   | method to be used to authenticate with PBI API, defaults to "user_account", also supports "service_principal"                      |
| _client_id_, _#client_secret_ | obtained upon application creation (see [Register Power BI App](https://docs.microsoft.com/cs-cz/power-bi/developer/register-app)) |
| tenant_id                     | ID of the tenant that your application resides in"                                                                                 |
| _username_, _#password_       | credentials used to login to Power BI service                                                                                      |
| _datasets_                    | array with datasets ids and groups                                                                                                 |
| _dataset_                     | dataset id as string                                                                                                               |
| _group_                       | can be found in URL in Power BI service                                                                                            |
| _notify_                      | specifies whether to receive e-mail notification upon refresh error (true) or not (false)                                          |


Sample config for username-password authentication:
```
jsonc
{
    "client_id": /* string */,
    "#client_secret": /* string, alias: client_secret */,
    "username": /* string */,
    "#password": /* string, alias: password */,
    "datasets": [
        {"dataset": /* string */, "group": /* string|null */}
        // additional datasets
    ],
    "notify": /* boolean */
}
```

Sample config for service principal authentication. Please note that the `notify` parameter is not supported when using `service_principal` auth method.

```
jsonc
{
    "auth_method": "service_principal",
    "client_id": /* string */,
    "#client_secret": /* string, alias: client_secret */,
    "tenant_id": /* string */,
    "datasets": [
        {"dataset": /* string */, "group": /* string|null */}
        // additional datasets
    ],
}
```



