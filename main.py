"""Main runner module
"""

import sys
import logging
import time
from typing import Dict, Set
from bizztreat_base.config import Config

from refresher.powerbi import Refresher
from refresher.models import RefreshFailedError, RefreshHasNotStartedError, RefreshRequest

logging.basicConfig(level=logging.INFO, stream=sys.stdout)
logger = logging.getLogger(__name__)


def main():
    """Main runner"""
    config = Config(force_schema=True).config
    password = config.get("#password", config.get("password", None))
    client_secret = config.get("#client_secret", config.get("client_secret"))
    refresh_start_timeout = config.get("refresh_start_timeout", 7200)

    client = Refresher(
        username=config.get("username", None),
        password=password,
        client_id=config["client_id"],
        client_secret=client_secret,
        auth_method=config.get("auth_method", "user_account"),
        tenant_id=config.get("tenant_id", None),
    )
    requests: Set[RefreshRequest] = set()
    request_start: Dict[RefreshRequest, float] = {}
    for dataset_config in config["datasets"]:
        dataset = dataset_config["dataset"]
        group = dataset_config.get("group", None)
        logger.info("Starting refresh for dataset '%s' in group '%s'", dataset, group or "no group")
        request = client.start_refresh(dataset, group, config.get("notify", False))
        requests.add(request)
        request_start[request] = time.time()

    logger.info("Waiting for all refreshes to finish")
    while requests:
        done = set()
        for request in requests:
            refresh = client.get_dataset_refresh(request.id, request.dataset, request.group)
            if refresh is None:
                if time.time() - request_start[request] >= refresh_start_timeout:
                    raise RefreshHasNotStartedError(request)
                logger.warning(
                    "No refresh was found for given request id '%s' yet.",
                    request.id,
                )
                continue
            if refresh.status.lower() == "completed":
                logger.info("Refresh was completed for dataset %s", request.dataset)
                done.add(request)
                continue
            if refresh.status.lower() == "unknown":
                continue
            logger.error("Dataset refresh is in unknown state - %s", refresh.status)
            raise RefreshFailedError(request)
        time.sleep(5)
        # Work around set changing its size during iteration in for loop
        requests -= done
    logger.info("All refresh operations are completed")


if __name__ == "__main__":
    main()
