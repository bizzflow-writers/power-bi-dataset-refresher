FROM python:3.10-slim

RUN rm -rf /var/lib/apt/lists/*

RUN apt-get update

RUN apt-get -y install curl

RUN curl -sSL https://install.python-poetry.org | python -

WORKDIR /code

ADD pyproject.toml .
ADD poetry.lock .

RUN POETRY_VIRTUALENVS_CREATE=false /root/.local/bin/poetry install --without dev --no-root

ADD . .

CMD ["python", "-u", "main.py", "--config-schema", "./config.schema.json"]
