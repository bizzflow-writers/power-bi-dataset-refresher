"""Models
"""

from dataclasses import dataclass
from typing import Any, Dict, Optional


@dataclass(frozen=True)
class RefreshRequest:
    id: str
    dataset: str
    group: Optional[str] = None


@dataclass
class Refresh:
    request_id: str
    status: str

    @staticmethod
    def from_api(content: Dict[str, Any]) -> "Refresh":
        """Create from api response"""
        return Refresh(request_id=content["requestId"], status=content["status"])


class RefreshHasNotStartedError(Exception):
    """Raised when refresh cannot be started within designated timeout"""

    def __init__(self, request: RefreshRequest) -> None:
        super().__init__(
            f"Refresh for dataset {request.dataset} (request id {request.id}) did not start within designated timeout"
        )


class RefreshFailedError(Exception):
    """Raised when refresh started but then failed due to an error"""

    def __init__(self, request: RefreshRequest) -> None:
        super().__init__(
            (
                f"Refresh of dataset {request.dataset} (request id {request.id}) failed. "
                "See Power BI service for more information"
            )
        )
