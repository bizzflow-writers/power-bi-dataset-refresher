"""Power BI Refresher
"""

import time
import logging
from typing import List, Optional
import requests
import backoff
from azure.identity import ClientSecretCredential

from refresher.models import Refresh, RefreshRequest

logger = logging.getLogger(__name__)


class Refresher:
    """Power BI Refresher"""

    scope = "openid"
    resource_user_account = "https://analysis.windows.net/powerbi/api"
    resource_service_principal = "https://analysis.windows.net/powerbi/api/.default"
    token_endpoint = "https://login.microsoftonline.com/common/oauth2/token"

    def __init__(
        self,
        client_id: str,
        client_secret: str,
        auth_method: str,
        username: str = None,
        password: str = None,
        tenant_id: str = None,
    ):
        self.username = username
        self.password = password
        self.client_id = client_id
        self.client_secret = client_secret
        self.tenant_id = tenant_id
        self.auth_method = auth_method

        self.session = requests.Session()
        self._refresh_token: Optional[str] = None
        self._access_token: Optional[str] = None

    @backoff.on_exception(
        backoff.expo, exception=(requests.HTTPError, KeyError, requests.ConnectionError), max_tries=5, max_time=60
    )
    def _get_service_principal_token(self):
        """Get access token using service principal authentication"""

        credentials = ClientSecretCredential(
            tenant_id=self.tenant_id, client_id=self.client_id, client_secret=self.client_secret
        )
        token = credentials.get_token(self.resource_service_principal)
        self._access_token = token.token
        return self._access_token

    @backoff.on_exception(
        backoff.expo, exception=(requests.HTTPError, KeyError, requests.ConnectionError), max_tries=5, max_time=60
    )
    def _get_token(self, refresh_token: Optional[str] = None) -> str:
        """Get token using refresh_token or by initial login"""
        form = {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "scope": self.scope,
            "resource": self.resource_user_account,
        }
        if refresh_token is not None:
            form.update({"refresh_token": refresh_token, "grant_type": "refresh_token"})
        else:
            form.update({"username": self.username, "password": self.password, "grant_type": "password"})
        response = self.session.post(self.token_endpoint, data=form)
        response.raise_for_status()
        content = response.json()
        self._refresh_token = content.get("refresh_token", self._refresh_token)
        self._access_token = content["access_token"]
        return self._access_token

    @property
    def access_token(self) -> str:
        """Get access token either by initial request or by refersh token (if any)"""

        if self._access_token is not None:
            return self._access_token

        if self.auth_method == "service_principal":
            return self._get_service_principal_token()

        if self._refresh_token is not None:
            try:
                logger.info("Obtaining access token using refresh token")
                return self._get_token(self._refresh_token)
            except (requests.HTTPError, KeyError):
                logger.error("Error trying to obtain access token using refresh token", exc_info=True)
        logger.info("Obtaining access token using password authentication")
        return self._get_token()

    @backoff.on_exception(
        backoff.expo,
        exception=(requests.HTTPError, requests.ConnectionError),
        max_tries=10,
        max_time=180,  # there was 60 before, i have no idea why
    )
    def request(self, method: str, url: str, **kwargs) -> requests.Response:
        """Send a request and return response"""
        headers = kwargs.pop("headers", {})
        headers.update({"Authorization": f"Bearer {self.access_token}"})
        response = self.session.request(method, url, headers=headers, **kwargs)
        try:
            response.raise_for_status()
        except requests.HTTPError:
            logger.error(response.text)
            if response.status_code in (401, 403):
                self._access_token = None
            if response.status_code == 429:
                sleep_time = 18  # The requested default delay is 9, but it is usually not ready.
                logger.info(f"Sleeping for {sleep_time} seconds due to 429 rate limit")
                time.sleep(sleep_time)
            raise
        return response

    @staticmethod
    def _build_refresh_url(dataset: str, group: Optional[str] = None) -> str:
        """Build refresh url using either group or no group"""
        if group is not None:
            group = f"/groups/{group}"
        else:
            group = ""
        return f"https://api.powerbi.com/v1.0/myorg{group}/datasets/{dataset}/refreshes"

    def list_dataset_refreshes(self, dataset: str, group: Optional[str] = None) -> List[Refresh]:
        """Get list of dataset refreshes"""
        url = self._build_refresh_url(dataset, group)
        response = self.request("GET", url)
        return [Refresh.from_api(line) for line in response.json().get("value", [])]

    def get_dataset_refresh(self, request_id: str, dataset: str, group: Optional[str] = None) -> Optional[Refresh]:
        """Get dataset refresh by request id"""
        refreshes = self.list_dataset_refreshes(dataset, group)
        for refresh in refreshes:
            if refresh.request_id == request_id:
                return refresh
        return None

    def start_refresh(self, dataset: str, group: Optional[str] = None, notify: bool = False) -> RefreshRequest:
        """Start refresh and return request id"""
        body = {"notifyOption": "MailOnFailure"} if notify else None
        url = self._build_refresh_url(dataset, group)
        response = self.request("POST", url, json=body)
        return RefreshRequest(id=response.headers["requestid"], dataset=dataset, group=group)
